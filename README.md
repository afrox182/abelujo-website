# Abelujo website

This is the repository for the Abelujo website at
[abelujo.cc](https://www.abelujo.cc).

It uses the [Lektor](https://www.getlektor.com/) static content management system.

To run:

```
$ lektor server # or make run
```

If you also want to update the webpack files, you need `npm` installed
and then run it like this:

```
$ lektor server -f webpack
```
