title: Lists
---
sort_key: 120
---
summary: Putting books in lists without touching the stock.
---
allow_comments: no
---
body:

Lists are a way to group cards together without affecting the
stock. Only defined actions will affect it.

Available actions are:

- choose a supplier for all the cards of this list (this doesn't
  affect the cards in stock yet).
- add all the cards to the command list (Commands menu),
- receive a parcel and compare it against this list. The cards'
  supplier will be set when we apply the inventory, not before.
- return all those cards to the chosen supplier. The cards will be
  removed from the stock. It saves a movement of stock in history.

We can also export their content in txt, csv or pdf.
