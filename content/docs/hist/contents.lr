title: Historique
---
sort_key: 130
---
summary: Historique des ventes, des entrées et quelques chiffres
---
allow_comments: no
---
body:

La page d'historique comporte deux onglets:

* les dernières ventes, avec quelques chiffres
* les dernières entrées de stock.

<!-- * les derniers déplacements. -->

## Historique des ventes

Cette vue permet de voir les dernières ventes effectuées, triées par
ordre anti-chronologique, avec quelques chiffres:

* le **chiffre d'affaires du mois** en cours
* le nombre **total de titres vendus**, d'exemplaires, le coût du panier moyen
* la liste des dix **meilleures ventes** du mois

Il est possible de **filtrer cette liste par mois et par fournisseur**.


<div class="thumbnail_img"> <img src="/historique-ventes.png" alt="Vue de l'historique des ventes" style="width: 1000px;"/> </div>


## Actions possibles

### Annuler une vente

Pour annuler une vente, il suffit de cliquer sur la croix en face de
son entrée dans l'historique. Une vente ne peut être annulée qu'une
seule fois.

### Exporter la sélection

Il est possible d'exporter la sélection courante aux formats txt et csv.
