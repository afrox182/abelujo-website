title: Gestion des dépôts
---
sort_key: 90
---
summary: Gestion des dépôts
---
allow_comments: no
---
body:

La gestion des dépôts est possible en tant que libraire (qui reçoit
des notices) autant qu'en tant qu'éditeur (qui envoie ces notices au
libraire).

La première page montre un tableau récapitulatif des dépôts en cours:
nom du dépôt, diffuseur ou éditeur associé, date du dernier état de
dépôt, nombre d'alertes liées à ce dépôt, quantitée actuelle de
notices, coût total actuel, et prochaine échéance.

Cliquer sur le nom d'un dépôt permet:

* de le modifier,
* de voir quelques informations plus précises, telles que
  * la liste des notices associées,
  * le nombre d'exemplaires vendus depuis le dernier état de dépôt,
  * le montant à payer à l'éditeur,
* et d'ajouter des notices.

Un bouton "Nouveau dépôt" permet d'en créer un nouveau.


## Les états de dépôts

On choisit de sauvegarder un état de dépôt lorsqu'on paie un éditeur.

Lorsqu'on regarde les informations d'un dépôt, on peut cliquer sur
"Sauvegarder cet état de dépôt". Cela a pour effet d'enregistrer la
date du jour comme la nouvelle date d'état de dépôt et de re-calculer
le nombre d'exemplaires vendus, la quantité de notices courante, et la
somme à payer à l'éditeur.
