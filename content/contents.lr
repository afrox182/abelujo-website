_model: poster
---
title: Abelujo, logiciel libre de librairies
---
body:

#### banner ####
image: header-librairie.png
----
height: 500
----
class: dark
----
contents:

<div class="container lektor-intro">
  <div class="row">
    <div class="col-md-8">
      <img src="abelujo-logo.png" alt="Abelujo" class="logo" width="400px">
      <h3 style="color: white;">
        Logiciel libre pour librairies
      </h3>
      <a class="demolink" href="http://5.196.70.6:8002/fr/" style="text-decoration: underline; background-color: gold; color: black;">Testez la démo en ligne ! (admin/admin)</a>
    </div>
    <div class="col-md-4 visible-md-block visible-lg-block">

      <ul class="badges">
        <li><a href="https://framasphere.org/people/4ac5fae0bed90133a3ed2a0000053625">
          <img
              src="http://www.unixstickers.com/image/data/stickers/diaspora/diaspora%20logo%20neg.png"
              width="50px"
              title="Nous donnons des nouvelles sur Diaspora"
              alt="Rejoignez-nous sur Diaspora"></a></li>

        <li>
          <a href="https://liberapay.com/vindarel/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>
        </li>

      </ul>
    </div>
  </div>
  <div class="install-row hide-for-windows">
    <pre>Intéressé-es ? Contactez-nous: <a href="mailto:contact@abelujo.cc">contact@abelujo.cc</a></pre>
  </div>
</div>
#### text-block ####
text:

## Aperçu des fonctionnalités

* <i class="feature-circle fa fa-book"></i> <strong> <a href="docs/new_card"> Recherche bibliographique de qualité</a> </strong> Nous récupérons vos recherches par mots-clef ou par isbn sur des sources internet de qualité.

* <i class="feature-circle fa fa-rocket"></i> <strong> <a href="docs/inventories/"> Inventaires</a> </strong>
Inventaires envoyés en quelques coups de douchette, par rayon ou par lieu.

* <i class="feature-circle fa fa-exchange"></i> <strong> <a href="docs/commands/"> Gestion des commandes</a> </strong>
  Envoi et réception de colis, avec factures et bordereaux de réception (fonctionnalité en cours de livraison).

* <i class="feature-circle fa fa-database"></i> <strong> <a href="docs/deposits/"> Gestion des dépôts</a> </strong> Pour connaître l'état du dépôt en un clic.

* <i class="feature-circle fa fa-pie-chart"></i> <strong> <a href="docs/stats/"> Statistiques</a> </strong>
  Les (meilleures) ventes du mois, le stock mort, la valeur du fonds et des dépôts, ...

* <i class="feature-circle fa fa-barcode"></i> **Fonctionne avec une douchette usb** Si vous en possédez une,
  vous pouvez biper le code barre à la place de taper au clavier, pour toutes les opérations.

* <i class="feature-circle fa fa-heart"></i> **Pour librairies**
   Abelujo peut être utilisé par des associations ou des
   professionnels. Nous aidons à l'interface avec Dilicom mais elle
   n'est pas automatique (vous pouvez passer commande à Dilicom en
   téléchargeant depuis Abelujo une liste d'isbn avec leurs quantités,
   au format adéquat pour le site de Dilicom).

* <i class="feature-circle fa fa-pencil"></i> **Pour éditeurs** La gestion des dépôts et du fonds,
  souvent réparti sur plusieurs lieux, est également pensée pour les éditeurs de livres ou magazines, qui *envoient* des exemplaires chez les libraires.
* <i class="feature-circle fa fa-home"></i> **Pour particuliers**
   Faites-vous plaisir. On attend aussi votre retour !

* <i class="feature-circle fa fa-film"></i> **CDs** et **DVDs**

* <i class="feature-circle fa fa-file"></i> **[Export en pdf](docs/lists#exporter-en-diffrents-formats)** ou en csv pour tableurs.

* <i class="feature-circle fa fa-globe"></i> **Et plus**
  [Historique](docs/hist/) des mouvements, [listes de livres](docs/lists/), gestion fine par lieu, import initial d'ISBNs,... tout est dans la documentation... et en cours de développement.

----
class: two-column-list
#### slideshow ####
slides:

##### slide #####
description:

Statistiques
-----
image: abelujo-stats.png

##### slide #####
description:

Recherche de notices, par mots-clefs ou isbn
-----
image: abelujo-search_fr.png

##### slide #####
description: Listes de livres et exports en pdf, txt ou csv (pour Dilicom)
-----
image: lists-pdf-export_fr.png

##### slide #####
description: Vente de notices, remises
-----
image: abelujo-sell_fr.png

##### slide #####
description: Historique des ventes et des entrées, statistiques
-----
image: historique-ventes_fr.png

#### text-block ####
text:

<div class="container">
  <div class="row">
    <div class="col-md-12">

<!-- Begin MailChimp Signup Form -->
<div id="mc_embed_signup" class="text-block text-block-centered">

<form action="//abelujo.us12.list-manage.com/subscribe/post?u=d0e4146dcccb946e4ac7c97a7&amp;id=19ff20256c" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
    <div id="mc_embed_signup_scroll">
	<h2 id="newsletter">Restez informé(e)</h2>
<span >
	<label for="mce-EMAIL">Adresse courriel </label>
	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
</span>
	<span id="mce-responses" class="clear">
		<div class="response" id="mce-error-response" style="display:none"></div>
		<div class="response" id="mce-success-response" style="display:none"></div>
	</span>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_d0e4146dcccb946e4ac7c97a7_19ff20256c" tabindex="-1" value=""></div>
    <span class="clear"><input type="submit" value="Inscription" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
    </span>
</form>
</span>

<!-- TODO pb avec footer blanc au lieu de gris -->

<!--End mc_embed_signup-->


## Ils nous font confiance ##

----
class: centered

#### text-block ####
text:


[**Antigone**](https://www.bibliothequeantigone.org/), bibliothèque et librairie associative à Grenoble (5000
livres en librairie), depuis juin 2018 pour l'inventaire de départ, depuis septembre pour… le reste. Étroite collaboration.

*«J’ai présenté le logiciel aux autres, tout le monde était ravi. Trop merci, ça va nous changer notre taf de librairie !»*


[**La Case à Kat**](https://www.facebook.com/BEALEX14470/), librairie
spécialisée, salon de thé et bar à vin à Courseulles sur Mer (14),
cliente depuis mai 2018.

Nathalie Hamel, de la librairie [**Entre Pages et Plage**](https://www.facebook.com/nathamel76/) à Etretat, utilise Abelujo au quotidien depuis février 2017:

> *«C'est une petite librairie généraliste, à ce jour j'ai un stock d'environ 2500 livres neufs. Je vends aussi un peu de livres d'occasion.
> Mon stock est constitué de livres pour adultes et jeunesse, littérature, bandes-dessinées, albums, quelques beaux livres, du régionalisme.
> Je me sers d'abelujo pour savoir ce que j'ai en stock, et donc pour les ventes, mais je ne m'en suis jamais servie pour les commandes. Cela fonctionne bien, dans la mesure où mon stock n'est pas très important, et où je travaille seule. C'est moi qui passe  les commandes  et qui les saisis dans le logiciel, dont je sais plus ou moins ce que j'ai en stock. Quand un client me demande si j'ai un livre, je vais voir en rayon.
> Je n'ai pas pris de logiciel pro, parce que 'en n'en avais pas les moyens au moment ou j'ai ouvert la librairie, et de serait encore difficile aujourd'hui. En effet, même si la librairie fonctionne, et quelle a été bien accueillie, je ne parviens pas à me verser un salaire et je dois être très vigilante sur mes dépenses. Disons que depuis l'ouverture, j'ai acheté plus de livres que je n'en ai vendus. La gestion du stock est tout un art et je suis en train de l'apprendre.»*

Les éditions [Nzoi](http://www.editions-nzoi.org/), pour gérer leur
stock réparti dans de nombreux lieux.

Nous avons nous-mêmes amené Abelujo dans une petite association
toulousaine, pour réaliser un inventaire en une matinée au lieu de
plusieurs semaines.

Abelujo est en sus en cours de tests à Marseille, Cahors, Pau ou encore
Barcelone.


#### text-block ####
text:

## Comment aider ##

Nous vous invitons à tester la démo en ligne et à nous faire part de vos
impressions.

Vous pouvez soutenir le développement et envoyer un signal de soutien
avec un don, même petit, sur Liberapay.

Vous pouvez enfin nous charger d'héberger votre instance personnelle.

<a href="https://liberapay.com/vindarel/donate"><img alt="Donate using Liberapay" src="https://liberapay.com/assets/widgets/donate.svg"></a>


----
class: centered

#### text-block ####
text:

## Installation

Abelujo est une application web. On peut l'installer sur sa machine
personnelle, mais c'est en l'installant sur un serveur web qu'on
pourra l'utiliser à plusieurs et depuis n'importe où.

Nous pouvons vous fournir votre version du logiciel en l'installant
sur nos serveurs. Comme la version de démonstration, mais pour
vous. Dans ce cas-là, **vous y accéderez simplement par un navigateur web**. Nous vous laisserons le temps de tester puis nous vous
demanderons une participation aux frais du serveur.

Abelujo s'installe uniquement sur un système d'exploitation GNU/Linux,
tel que [LinuxMint](http://www.linuxmint.com),
[Debian](https://debian-facile.org/),
[Ubuntu](https://www.ubuntu-fr.org/)…

Les instructions d'installation complètes sont disponibles sur
[la forge de développement](https://gitlab.com/vindarel/abelujo),
aux côtés des sources du logiciel.


----
class: centered

#### text-block ####
text:

## Logiciel Libre

Abelujo est un logiciel libre (mais
[qu'est-ce que c'est ?](https://framastart.org/)) publié sous licence
GNU GPLv3, développé en Python et Javascript. Les sources sont visibles
[sur Gitlab](https://gitlab.com/vindarel/abelujo), accompagnées de
leur [documentation technique](http://dev.abelujo.cc).

Abelujo veut dire "ruche" en espéranto et on prononce "abelouyo" !

----
class: centered

#### text-block ####
text:

## Contact ##

Envoyez-nous
<a href="mailto:conxtact@abelujox.cc"
    onmouseover="this.href=this.href.replace(/x/g,'');">un email</a>.


Nous sommes sur [Diaspora](https://framasphere.org/people/4ac5fae0bed90133a3ed2a0000053625)…

… et sur [Facebook](https://www.facebook.com/Abelujo-logiciel-libre-de-gestion-de-librairie-2285856431431952/).

----
class: centered
